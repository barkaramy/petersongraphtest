let way = 'ABBECCD';
let wayList = way.split('');

//get the code ascii of the different vertices of the string
let ord = (str) => {
  return str.charCodeAt();
};

// the function to find is there is a path or no
let findPath = (path, v) => {
  let edges = [v];
  let mat = [...Array(10).keys()].map((i) =>
    [...Array(10).keys()].map((j) => false)
  );
  mat[0][1] = mat[1][2] = mat[2][3] = mat[3][4] = mat[4][0] = mat[0][5] = mat[1][6] = mat[2][7] = mat[3][8] = mat[4][9] = mat[5][7] = mat[7][9] = mat[9][6] = mat[6][8] = mat[8][5] = true;

  for (let i = 1; i < path.length; i++) {
    if (mat[v][ord(path[i]) - ord('A')] || mat[ord(path[i]) - ord('A')][v]) {
      v = ord(path[i]) - ord('A');
    } else if (
      mat[v][ord(path[i]) - ord('A') + 5] ||
      mat[ord(path[i]) - ord('A') + 5][v]
    ) {
      v = ord(path[i]) - ord('A') + 5;
    } else {
      return false;
    }
    edges.push(v);
  }

  return edges;
};

let externalWalk = findPath(wayList, ord(wayList[0]) - ord('A'));
let internalWalk = findPath(wayList, ord(wayList[0]) - ord('A') + 5);

if (externalWalk !== false) {
  console.log(externalWalk.join());
}
if (internalWalk !== false) {
  console.log(internalWalk.join());
}
if (externalWalk === false && internalWalk === false) {
  console.log(-1);
}
